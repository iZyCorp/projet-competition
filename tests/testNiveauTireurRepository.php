<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $repo = new Repositories\NiveauTireurRepository($db);

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des niveaux tireurs :');

    $tab = array (
        'libNivTireur' => 'PRO'

    );

    $niveau = new Entities\NiveauTireur($tab);

    $res = $repo->save($niveau);
    dump_var($res, DUMP, "ajout d'un niveau tireur");

    $tab = array (
        'libNivTireur' => 'Incroyable'

    );
    $niveau = new Entities\NiveauTireur($tab);

    $res = $repo->save($niveau);
    dump_var($res, DUMP, "mise à jour d'un niveau tireur");

   $res = $repo->getById(1);
   dump_var($res, DUMP, "information du club niveau tireur 1");

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des niveaux tireurs :');
}