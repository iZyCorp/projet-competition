<?php
declare(strict_types=1);

use Repositories\CompetitionRepository;

define('DUMP', true);

require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

$CompetitionRepository = new CompetitionRepository($db);
dump_var($CompetitionRepository, DUMP, 'Objet CompetitionRepository:');

// Initializing a default Competition object for test
$CompetitionTestObject = new \Entities\Competition(array(
    'idCompet' => null,
    'dateDebutCompet' => DateTime::createFromFormat('Y-m-d', '2019-01-01'),
    'dateFinCompet' => DateTime::createFromFormat('Y-m-d', '2019-01-01'),
    'idClubOrganisateur' => 1,
));
dump_var($CompetitionTestObject, DUMP, 'Objet CompetitionTestObject:');

// Testing different methods
$CompetitionRepository->getById(1);
$CompetitionRepository->getAll();
$CompetitionRepository->insert($CompetitionTestObject);
$CompetitionRepository->update($CompetitionTestObject);