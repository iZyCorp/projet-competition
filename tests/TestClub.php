<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\Club;

echo '<h1>Instanciation par défaut</h1>';
$obj = new Club();
dump_var($obj, DUMP, 'Club par défaut:');

$tab = array (
    'idClub'=>1,
    'nomClub'=>"XIE XU",
    'adresseClub'=>"130 rue de l'ort",
    'cpClub' => '69008',
    'villeClub' => 'Lyon'

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Club($tab);
dump_var($obj, DUMP, 'Club avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdClub(2);
dump_var($obj, DUMP, 'Club modifier:');

