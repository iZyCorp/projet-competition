<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

use Entities\Tireur;

$clubRepo = new ClubRepository($db);

echo '<h1>Instanciation par défaut</h1>';
$obj = new Tireur();
dump_var($obj, DUMP, 'Tireur par défaut:');

$tab = array (
    'nomTireur' => 'KEBIR',
    'prenomTireur' => 'KAMEL',
    'dateNaissTireur' => '1997-02-07',
    'numLicenceTireur' => '19362',
    'sexeTireur' => 'M',
    'poidsTireur' => 90.00,
    'idClub' => 2,
    'idNivTireur' => 1

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Tireur($tab);

dump_var($obj, DUMP, 'Tireur avec toutes les valeurs:');


