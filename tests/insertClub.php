<?php

declare(strict_types=1);

define('DUMP', true);

require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if (!is_null($db)) {
    $newClub = array('nomClub' => "La baston", 'adresseClub' => "6543 Rue de la teuf", 'cpClub' => 86443, 'villeClub' => "Lyon");
    $res = insertClub($db, $newClub);
    dump_var($res, DUMP, 'résultat:');
} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}