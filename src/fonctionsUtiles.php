<?php

/*
 * Exercice 1 Question 2 : Faire la fonction pour ce connecter à la BDD
 * Crée la connexion PDO de votre application
 * @param array $infoBdd tableau qui contient les informations de connexion de la BDD, c'est celui du fichier appConfig
 * @return PDO Un objet de type PDO
 */
function connectBdd(array $infoBdd): ?\PDO {
    $db = null;

    // Récupération des informations de votre table infoBdd, il fortement conseillé d'utiliser des variables, la première vous est donnée
    $myinterface = ($infoBdd['interface']);
    $mytype = ($infoBdd['type']);
    $myhost = ($infoBdd['host']);
    $myport = ($infoBdd['port']);
    $mycharset = ($infoBdd['charset']);
    $mydbname = ($infoBdd['dbname']);
    $myuser = ($infoBdd['user']);
    $mypass = ($infoBdd['pass']);

    //  Composition du DSN

    $dsn = "{$mytype}:host={$myhost};dbname={$mydbname};charset={$mycharset}";

    //  Instanciation de PDO (le \ pour l'espace de nom racine... peut être utile par la suite)

    $db = new PDO($dsn, $myuser, $mypass);

    // renvoi de votre object PDO
    return $db;
}


/*
 * Exercice 2 - Question 1 : Faire la fonction getAllClubs qui permet de récupèrer l'ensemble des clubs présent en BDD
 * Récupère tous les clubs depuis la BDD
 * @param PDO $bdd
 * @return array|null Un tableau indéxé de tableaux associatifs
 */
function getAllClubs(\PDO $bdd): ?array {
    $resultSet = NULL;

    // Créer la requête SQL qui va permettre de récupérer tous les clubs
    $req = 'SELECT * FROM club';
    //Exécution de la rêquete
    $res = $bdd->query($req);

    //Si votre requête renvoie quelque chose, parcourez le résultat et insérer le dans $resultSet;
    if ($res) {
        while ($row = $res->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet[] = $row;
        }
    }

    // renvoi de votre tableau contenant l'ensemble des clubs
    return $resultSet;
}

/*
 * Exercice 3 - Question 1
 * Insère un nouveau club dans la bdd
 * @param PDO $bdd
 * @param array $club tableau normalement sans clé id
 * @return array|null le tableau complété avec l'id
 */
function insertClub(\PDO $bdd, array $club): ?array {
    dump_var($club, DUMP, '$club dans insertClub');
    $resultSet = NULL;
    // Créer votre requête avec quote, on ne s'occupe pas des informations qui viennent des clés étrangère et on laisse l'auto_incremente se gérer de la clé primaire
    $req = "INSERT INTO club (nomClub, adresseClub, cpClub, villeClub) VALUES (:nom, :adresse, :cp, :ville); ";

    //Vérifier votre req
    $reqPrep = $bdd->prepare($req);

    //Exécuter votre requête
    
    $res = $reqPrep->execute(
        [':nom' => $club['nomClub'],
        ':adresse' => $club['adresseClub'],
        ':cp' => $club['cpClub'],
        ':ville' => $club['villeClub']
        ]);

    //Récupèrer l'id généré et l'ajouter à votre tableau
    $id = $bdd->lastInsertId();

    //Ajout de l'id du club dans notre tableau $club
     $club['idClub'] = $id;

    //retourner le tableau maj avec l'id
    return $club;
}

/*
 * Exercice 4 - Question 1
 * Récupère le club à partir du n° id
 * @param PDO $bdd
 * @param int $id
 * @return array|null
 */
function getClubId(PDO $bdd, int $id): ?array {
    $resultSet = NULL;
    // Créer votre requête en utilisant une requête préparée
    $req = "SELECT * FROM club WHERE idClub = '{$id}'";

    // Exécution de votre requête
    $res = $bdd->query($req);

    //Récupération du résultat dans votre resultSet si l'éxécution c'est bien passée
    if ($res) {
        while ($row = $res->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet[] = $row;
        }
    }

    //retourner le Club rechercher
    return $resultSet;
}

/*
 * Exercice 5 - Question 1
 * Modification d'un club existant
 * @param PDO $bdd
 * @param array $club un tableau avec la clé id
 * @return array|null le club modifié
 */
function updateClub(\PDO $bdd, array $club): ?array {
    dump_var($club, DUMP, '$club dans updateClub');
    $resultSet = NULL;

    $idClub = $club["idClub"];
    // On test l'id du club, si ce dernier est vide ou null alors on insert le club sinon on le modifie ==> partie optimisation
    if ($idClub != null) {

        $req = "UPDATE club SET nomClub = :nomClub, adresseClub = :adresseClub, cpClub = :cpClub, villeClub = :villeClub WHERE idClub = $idClub";

        $reqPrep = $bdd->prepare($req);

        $res = $reqPrep->execute(
            [':nomClub' => $club['nomClub'],
                ':adresseClub' => $club['adresseClub'],
                ':cpClub' => $club['cpClub'],
                ':villeClub' => $club['villeClub']
            ]);

        $resultSet = $club;
    }

    //retourner le Club maj
    return $resultSet;

}

/**
 * exercice 6 - Question 1
 * Fait un insert ou un update du club selon la clé id
 * @param PDO $bdd
 * @param array $club
 * @return array|null
 */
function saveClub(PDO $bdd, array $club): ?array
{
    if (isset($club['idClub'])) {

        $resultSet = updateClub($bdd, $club);

    } else {
        $resultSet = insertClub($bdd, $club);
    }
    return $resultSet;
}


