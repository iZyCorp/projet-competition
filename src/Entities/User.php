<?php

namespace Entities;

class User {

    private $username;
    private $password;
    private $idUser;

    /**
     * @return mixed
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getIdUser() {
        return $this->idUser;
    }

    /**
     * @param mixed $id
     */
    public function setIdUser($idUser): void {
        $this->idUser = $idUser;
    }

    public function __construct(array $datas = NULL) {
        if (!is_null($datas)) {
            (isset($datas['idUser'])) ? $this->setIdUser($datas['idUser']) : $this->setIdUser(null);
            (isset($datas['username'])) ? $this->setUsername($datas['username']) : $this->setUsername(null);
            (isset($datas['password'])) ? $this->setPassword($datas['password']) : $this->setPassword(null);
        }
    }
}