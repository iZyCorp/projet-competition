<?php

namespace Entities;

class Participer
{
    private $idTireur = null;
    private $idRencontre;
    private $gagnant;
    private $coin;

    /**
     * @return mixed
     */
    public function getIdTireur() : ?int
    {
        return $this->idTireur;
    }
    /**
     * @return mixed
     */
    public function getIdRencontre() : string
    {
        return $this->idRencontre;
    }

    /**
     * @return mixed
     */
    public function getGagnant() : string
    {
        return $this->gagnant;
    }
    /**
     * @return mixed
     */
    public function getCoinParticiper() : string
    {
        return $this->coin;
    }

    /**
     * @param mixed $idTireur
     */
    public function setIdTireur($idClub): void
    {
        if($this->idTireur==NULL) {
            $this->idTireur = $idClub;
        }
    }

    /**
     * @param mixed $idRencontre
     */
    public function setIdRencontre($idRencontre): void
    {
        $this->idRencontre = $idRencontre;
    }

    /**
     * @param mixed $gagnantParticiper
     */
    public function setGagnantParticiper($gagnantParticiper): void
    {
        $this->gagnant = $gagnantParticiper;
    }


    /**
     * @param mixed $coinParticiper
     */
    public function setCoinParticiper($coinParticiper): void
    {
        $this->coinParticiper = $coinParticiper;
    }

    /**
     * @return mixed
     */

    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idTireur'])) ? $this->setIdTireur($datas['idTireur']) : $this->setIdTireur(null);
            (isset($datas['idRencontre'])) ? $this->setIdRencontre($datas['idRencontre']) : $this->setIdRencontre(null);
            (isset($datas['gagnant'])) ? $this->setGagnantParticiper($datas['gagnant']) : $this->setGagnantParticiper('');
            (isset($datas['coin'])) ? $this->setCoinParticiper($datas['coin']) : $this->setCoinParticiper('');
        }
    }
}