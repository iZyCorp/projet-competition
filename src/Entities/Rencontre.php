<?php

namespace Entities;

use DateTime;

class Rencontre {

    private ?int $idRencontre = null;
    protected DateTime $heureDebutRencontre;
    private DateTime $heureFinRencontre;
    private int $idCompet;

    /**
     * @return mixed
     */
    public function getIdRencontre(): int {
        return $this->idRencontre;
    }

    /**
     * @param mixed $idRencontre
     */
    public function setIdRencontre($idRencontre): void {
        $this->idRencontre = $idRencontre;
    }

    /**
     * @return mixed
     */
    public function getHeureDebutRencontre(): DateTime {
        return $this->heureDebutRencontre;
    }

    /**
     * @param mixed $heureDebutRencontre
     */
    public function setHeureDebutRencontre(DateTime $heureDebutRencontre): void {
        $this->heureDebutRencontre = $heureDebutRencontre;
    }

    /**
     * @return mixed
     */
    public function getHeureFinRencontre(): DateTime {
        return $this->heureFinRencontre;
    }

    /**
     * @param mixed $heureFinRencontre
     */
    public function setHeureFinRencontre(DateTime $heureFinRencontre): void {
        $this->heureFinRencontre = $heureFinRencontre;
    }

    /**
     * @return mixed
     */
    public function getIdCompet(): int {
        return $this->idCompet;
    }

    /**
     * @param mixed $idCompet
     */
    public function setIdCompet($idCompet): void {
        $this->idCompet = $idCompet;
    }

    public function __construct(array $datas = NULL) {
        if (!is_null($datas)) {
            (isset($datas['idRencontre'])) ? $this->setIdRencontre($datas['idRencontre']) : $this->setIdRencontre(null);
            (isset($datas['heureDebutRencontre'])) ? $this->setHeureDebutRencontre(DateTime::createFromFormat('Y-m-d H:i:s', $datas['heureDebutRencontre'])) : $this->setHeureDebutRencontre(DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00'));
            (isset($datas['heureFinRencontre'])) ? $this->setHeureFinRencontre(DateTime::createFromFormat('Y-m-d H:i:s', $datas['heureFinRencontre'])) : $this->setHeureFinRencontre(DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00'));
            (isset($datas['idCompet'])) ? $this->setIdCompet($datas['idCompet']) : $this->setIdCompet(null);
        }
    }
}
