<?php


namespace Entities;


class NiveauTireur
{
    private $idNivTireur = NULL;
    private $libNivTireur;

    /**
     * @return mixed
     */
    public function getIdNivTireur() : ?int
    {
        return $this->idNivTireur;
    }

    /**
     * @param null $idNivTireur
     */
    public function setIdNivTireur($idNivTireur): void
    {
        $this->idNivTireur = $idNivTireur;
    }

    /**
     * @return mixed
     */
    public function getLibNivTireur(): string
    {
        return $this->libNivTireur;
    }

    /**
     * @param mixed $libNivTireur
     */
    public function setLibNivTireur($libNivTireur): void
    {
        $this->libNivTireur = $libNivTireur;
    }

    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idNivTireur'])) ? $this->setIdNivTireur($datas['idNivTireur']) : $this->setIdNivTireur(null);
            (isset($datas['libNivTireur'])) ? $this->setLibNivTireur($datas['libNivTireur']) : $this->setLibNivTireur('');
        }

    }

}

?>