<?php

namespace Entities;

use DateTime;

class Competition {

    private ?int $idCompet = null;
    private DateTime $dateDebutCompet;
    private DateTime $dateFinCompet;
    private int $idClubOrganisateur;

    /**
     * @return int
     */
    public function getIdCompet(): int {
        return $this->idCompet;
    }

    /**
     * @param int $idCompet
     */
    public function setIdCompet(?int $idCompet): void {
        if($this->idCompet==NULL) {
            $this->idCompet = $idCompet;
        }
    }

    /**
     * @return DateTime
     */
    public function getDateDebutCompet(): DateTime {
        return $this->dateDebutCompet;
    }

    /**
     * @param DateTime $dateDebutCompet
     */
    public function setDateDebutCompet(DateTime $dateDebutCompet): void {
        $this->dateDebutCompet = $dateDebutCompet;
    }

    /**
     * @return DateTime
     */
    public function getDateFinCompet(): DateTime {
        return $this->dateFinCompet;
    }

    /**
     * @param DateTime $dateFinCompet
     */
    public function setDateFinCompet(DateTime $dateFinCompet): void {
        $this->dateFinCompet = $dateFinCompet;
    }

    /**
     * @return mixed
     */
    public function getIdClubOrganisateur() {
        return $this->idClubOrganisateur;
    }

    /**
     * @param mixed $idClubOrganisateur
     */
    public function setIdClubOrganisateur($idClubOrganisateur): void {
        $this->idClubOrganisateur = $idClubOrganisateur;
    }



    public function __construct(array $datas = NULL) {
        if (!is_null($datas)) {
            (isset($datas['idCompet'])) ? $this->setIdCompet($datas['idCompet']) : $this->setIdCompet(null);
            (isset($datas['dateDebutCompet'])) ? $this->setDateDebutCompet(DateTime::createFromFormat('Y-m-d', $datas['dateDebutCompet'])) : $this->setDateDebutCompet(DateTime::createFromFormat('Y-m-d', '0000-00-00'));
            (isset($datas['dateFinCompet'])) ? $this->setDateFinCompet(DateTime::createFromFormat('Y-m-d', $datas['dateFinCompet'])) : $this->setDateFinCompet(DateTime::createFromFormat('Y-m-d', '0000-00-00'));
            (isset($datas['idClubOrganisateur'])) ? $this->setIdClubOrganisateur($datas['idClubOrganisateur']) : $this->setIdClubOrganisateur(null);
        }
    }

}