<?php


namespace Entities;

class Tireur {

    private $idTireur = NULL;
    private $nomTireur;
    private $prenomTireur;
    private $dateNaissTireur;
    private $numLicenceTireur;
    private $sexeTireur;
    private $poidsTireur;
    private $idClub;
    private $idNivTireur;

    /**
     * @return null
     */
    public function getIdTireur(): ?int {
        return $this->idTireur;
    }

    /**
     * @param null $idTireur
     */
    public function setIdTireur($idTireur): void {
        $this->idTireur = $idTireur;
    }

    /**
     * @return mixed
     */
    public function getNomTireur(): string {
        return $this->nomTireur;
    }

    /**
     * @param mixed $nomTireur
     */
    public function setNomTireur($nomTireur): void {
        $this->nomTireur = $nomTireur;
    }

    /**
     * @return mixed
     */
    public function getPrenomTireur(): string {
        return $this->prenomTireur;
    }

    /**
     * @param mixed $prenomTireur
     */
    public function setPrenomTireur($prenomTireur): void {
        $this->prenomTireur = $prenomTireur;
    }

    /**
     * @return mixed
     */
    public function getDateNaissTireur(): string {
        return $this->dateNaissTireur;
    }

    /**
     * @param mixed $dateNaissTireur
     */
    public function setDateNaissTireur($dateNaissTireur): void {
        $this->dateNaissTireur = $dateNaissTireur;
    }

    /**
     * @return mixed
     */
    public function getNumLicenceTireur(): string {
        return $this->numLicenceTireur;
    }

    /**
     * @param mixed $numLicenceTireur
     */
    public function setNumLicenceTireur($numLicenceTireur): void {
        $this->numLicenceTireur = $numLicenceTireur;
    }

    /**
     * @return mixed
     */
    public function getSexeTireur(): string {
        return $this->sexeTireur;
    }

    /**
     * @param mixed $sexeTireur
     */
    public function setSexeTireur($sexeTireur): void {
        $this->sexeTireur = $sexeTireur;
    }

    /**
     * @return mixed
     */
    public function getPoidsTireur(): float {
        return $this->poidsTireur;
    }

    /**
     * @param mixed $poidsTireur
     */
    public function setPoidsTireur($poidsTireur): void {
        $this->poidsTireur = $poidsTireur;
    }

    /**
     * @return mixed
     */
    public function getIdClub(): ?int {
        return $this->idClub;
    }

    /**
     * @param mixed $idClub
     */
    public function setIdClub($idClub): void {
        $this->idClub = $idClub;
    }

    /**
     * @return mixed
     */
    public function getIdNivTireur(): ?int {
        return $this->idNivTireur;
    }

    /**
     * @param mixed $idNivTireur
     */
    public function setIdNivTireur($idNivTireur): void {
        $this->idNivTireur = $idNivTireur;
    }

    public function __construct(array $datas = NULL) {
        if (!is_null($datas)) {
            (isset($datas['idTireur'])) ? $this->setIdTireur($datas['idTireur']) : $this->setIdTireur(null);
            (isset($datas['nomTireur'])) ? $this->setNomTireur($datas['nomTireur']) : $this->setNomTireur('');
            (isset($datas['prenomTireur'])) ? $this->setPrenomTireur($datas['prenomTireur']) : $this->setPrenomTireur('');
            (isset($datas['dateNaissTireur'])) ? $this->setDateNaissTireur($datas['dateNaissTireur']) : $this->setDateNaissTireur('');
            (isset($datas['numLicenceTireur'])) ? $this->setNumLicenceTireur($datas['numLicenceTireur']) : $this->setNumLicenceTireur('');
            (isset($datas['sexeTireur'])) ? $this->setSexeTireur($datas['sexeTireur']) : $this->setSexeTireur('');
            (isset($datas['poidsTireur'])) ? $this->setPoidsTireur($datas['poidsTireur']) : $this->setPoidsTireur('');
            (isset($datas['idClub'])) ? $this->setIdClub($datas['idClub']) : $this->setIdClub('');
            (isset($datas['idNivTireur'])) ? $this->setIdNivTireur($datas['idNivTireur']) : $this->setIdNivTireur('');
        }
    }
}