<?php

namespace Repositories;

use Entities\Tireur;

class TireurRepository
{
    //attribut qui contient la connexion à la BDD
    protected $bdd;

    //constructeur permet d'aboir la chaine de connexion PDO
    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    /* fonction qui donne la liste des clubs
     * @return array|null
     */

    public function getAll() : ? array
    {
        $resultSet = NULL;
        $query = 'SELECT * FROM tireur';

        $rqtResult = $this->bdd->query($query);

        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                //A chaque occurence de la BDD on crée un objet acteur qu'on insère dans le tableau resultSet
                $resultSet[] = new Tireur($row);

            }
        }

        return $resultSet;
    }

    /* fonction qui recherche un club
     * @param int $id
     * @return Club|null
     */

    public function getById( int $id): ?Tireur {
        $resultSet = NULL;
        $query = 'SELECT * FROM tireur WHERE idTireur =:idTireur;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idTireur' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new Tireur($tab);
            }
        }
        return $resultSet;
    }

    /* Fonction d'insertion d'un club
     * @param Tireur $entity
     * @return Club|null
     */

    public function insert(Tireur $entity): ?Tireur {
        $resultSet = NULL;


        $query = "INSERT INTO tireur" .
            " ( nomTireur, prenomTireur, dateNaissTireur, numLicenceTireur, sexeTireur, poidsTireur, idClub, idNivTireur)"
            . " VALUES (:nom, :prenom, :datenaiss, :numlicence, :sexe, :poids, :idclub, :idnivtireur )";
        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':nom' => $entity->getNomTireur(),
                ':prenom' => $entity->getPrenomTireur(),
                ':datenaiss' => $entity ->getDateNaissTireur(),
                ':numlicence' => $entity->getNumLicenceTireur(),
                ':sexe' => $entity->getSexeTireur(),
                ':poids' => $entity->getPoidsTireur(),
                ':idclub' => $entity->getIdClub(),
                ':idnivtireur' => $entity->getIdNivTireur()
            ]
        );

        if ($res !== FALSE) {
            //Si la requête c'est bien éxécuté on récupère l'id généré en BDD et on met à jour l'id dans $entity
            $entity->setIdTireur($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    /*Fonction de mise à jour d'un club
     * @param Tireur $entity
     * @return Club|null
     */
    public function update(Tireur $entity): ?Tireur {
        $resultSet = NULL;
        // On exécute l'update que si $entity a bien un numAct et que ce dernier existe en BDD
        if (is_null($entity->getIdTireur()) || is_null($this->getById($entity->getIdTireur()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE tireur"
                . " SET nomTireur=:nom, "
                . " prenomTireur=:prenom, "
                . " dateNaissTireur=:datenaiss, "
                . " numLicenceTireur=:numlicence, "
                . " sexeTireur=:sexe, "
                . " poidsTireur=:poids, "
                . " idClub=:idclub, "
                . " idNivTireur=:idnivtireur "
                . " WHERE idTireur = :id";

            // on prepare la requête
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Club');
            $res = $reqPrep->execute(
                [
                    ':nom' => $entity->getNomTireur(),
                    ':prenom' => $entity->getPrenomTireur(),
                    ':datenaiss' => $entity ->getDateNaissTireur(),
                    ':numlicence' => $entity->getNumLicenceTireur(),
                    ':sexe' => $entity->getSexeTireur(),
                    ':poids' => $entity->getPoidsTireur(),
                    ':idclub' => $entity->getIdClub(),
                    ':idnivtireur' => $entity->getIdNivTireur()
                ]
            );

            if ($res !== FALSE) {
                // si tout c'est bien passé on met l'entité qui viens d'être mis à jour dans resultSet
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    /**
     * Fait un insert ou un update du site selon la clé du Club
     * @param Tireur $entity
     * @return array|null
     */
    function save(Tireur $entity): ?Tireur {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getIdTireur())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }

}