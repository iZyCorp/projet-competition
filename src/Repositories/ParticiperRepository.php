<?php

namespace Repositories;

use Entities\Participer;

class ParticiperRepository
{
    protected $bdd;

    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    public function getAll() : ? array
    {
        $resultSet = NULL;
        $query = 'SELECT * FROM Participer';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new Participer($row);

            }
        }

        return $resultSet;
    }

    public function getById( int $idtireur,int $idrencontre): ?Participer {
        $resultSet = NULL;
        $query = 'SELECT * FROM participer WHERE IdTireur=:IdTireur and IdRencontre=:IdRencontre';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':IdTireur' => $idtireur, ':IdRencontre' => $idrencontre]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                $resultSet = new Participer($tab);
            }
        }
        return $resultSet;
    }

    public function insert(Participer $entity): ?Participer {
        $resultSet = NULL;

        $query = "INSERT INTO participer" .
            " (gagnant, gagnant)"
            . " VALUES (:Coin, :Gagnat )";
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':Coin' => $entity->getCoin(),
                ':Gagnant' => $entity->getGagnant()
            ]
        );

        if ($res !== FALSE) {
            $entity->setCoin($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    public function update(Participer $entity): ?Participer {
        $resultSet = NULL;
        if (is_null($entity->getCoin()) || is_null($this->getById($entity->getIdTireur(),$entity->getIdRencontre()))) {
            $resultSet = NULL;
        } else {

            $query = "UPDATE participer"
                . " SET Gagnant=:Gagnant  "
                . " WHERE Coin=:Coin,";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Participer');
            $res = $reqPrep->execute(
                [
                    ':Coin' => $entity->getCoin(),
                    ':Gagnant' => $entity->getGagnant()
                ]
            );

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    function save(Participer $entity): ?Participer {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getCoin())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }
}