<?php

namespace Repositories;

use Entities\Club;

class ClubRepository
{
    //attribut qui contient la connexion à la BDD
    protected $bdd;

    //constructeur permet d'aboir la chaine de connexion PDO
    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    /* fonction qui donne la liste des clubs
     * @return array|null
     */

    public function getAll() : ? array {
        $resultSet = NULL;
        $query = 'SELECT * FROM club';

        $rqtResult = $this->bdd->query($query);

        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                //A chaque occurence de la BDD on crée un objet acteur qu'on insère dans le tableau resultSet
                $resultSet[] = new Club($row);
            }
        }
        return $resultSet;
    }

    /* fonction qui recherche un club
     * @param int $id
     * @return Club|null
     */

    public function getById( int $id): ?Club {
        $resultSet = NULL;
        $query = 'SELECT * FROM Club WHERE idClub =:idClub;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idClub' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new Club($tab);
            }
        }
        return $resultSet;
    }

    /* Fonction d'insertion d'un club
     * @param Club $entity
     * @return Club|null
     */

    public function insert(Club $entity): ?Club {
        $resultSet = NULL;


        $query = "INSERT INTO club" .
            " ( nomClub, adresseClub, cpClub, villeClub)"
            . " VALUES (:nom, :adresse, :cp, :ville )";
        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':nom' => $entity->getNomClub(),
                ':adresse' => $entity->getAdresseClub(),
                ':cp' => $entity ->getCpClub(),
                ':ville' => $entity->getVilleClub(),
            ]
        );

        if ($res !== FALSE) {
            //Si la requête c'est bien éxécuté on récupère l'id généré en BDD et on met à jour l'id dans $entity
            $entity->setIdClub($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    /*Fonction de mise à jour d'un club
     * @param Club $entity
     * @return Club|null
     */
    public function update(Club $entity): ?Club {
        $resultSet = NULL;
        // On exécute l'update que si $entity a bien un numAct et que ce dernier existe en BDD
        if (is_null($entity->getIdClub()) || is_null($this->getById($entity->getIdClub()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE club SET nomClub=:nom, adresseClub=:adresse, cpClub=:cp, villeClub=:ville WHERE idClub = :id";

            // on prepare la requête
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Club');
            $res = $reqPrep->execute(
                [
                    ':nom' => $entity->getNomClub(),
                    ':adresse' => $entity->getAdresseClub(),
                    ':cp' => $entity ->getCpClub(),
                    ':ville' => $entity->getVilleClub(),
                    ':id' => $entity->getIdClub()
                ]
            );

            if ($res !== FALSE) {
                // si tout c'est bien passé on met l'entité qui viens d'être mis à jour dans resultSet
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    /**
     * Fait un insert ou un update du site selon la clé du Club
     * @param Club $entity
     * @return array|null
     */
    function save(Club $entity): ?Club {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getIdClub())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }

}