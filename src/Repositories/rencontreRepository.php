<?php

namespace Repositories;

use Entities\Rencontre;

class RencontreRepository
{
//attribut qui contient la connexion à la BDD
    protected $bdd;

    //constructeur permet d'aboir la chaine de connexion PDO
    public function __construct(\PDO $bdd){
        if(!is_null($bdd)) $this->bdd = $bdd;
    }

    /* fonction qui donne la liste des clubs
     * @return array|null
     */

    public function getAll() : ? array {
        $resultSet = NULL;
        $query = 'SELECT * FROM rencontre';

        $rqtResult = $this->bdd->query($query);

        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new Rencontre($row);
            }
        }
        return $resultSet;
    }

    /* fonction qui recherche un club
     * @param int $id
     * @return Club|null
     */

    public function getById( int $id): ?rencontre {
        $resultSet = NULL;
        $query = 'SELECT * FROM rencontre WHERE idRencontre =:idRencontre;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idRencontre' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new rencontre($tab);
            }
        }
        return $resultSet;
    }
    public function getRencontreByCompetition(int $id): ?array{
        $resultset= null ;
        $query= 'SELECT r.idRencontre, r.heureDebutRencontre, r.heureFinRencontre, r.idCompet
FROM rencontre r
INNER JOIN competition c ON c.idCompet = 1 AND c.idCompet = r.idCompet;';
        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {

                $resultSet[] = new Rencontre($row);
            }

        }

        return $resultSet;
    }



    /* Fonction d'insertion d'un club
     * @param Club $entity
     * @return Club|null
     */

    public function insert(rencontre $entity): ?rencontre {
        $resultSet = NULL;

        $query = "INSERT INTO rencontre(heureDebutRencontre, heureFinRencontre, idCompet) VALUES (:heureDebutRencontre, :heureFinRencontre, :idCompet)";
        $preparedStatement = $this->bdd->prepare($query);

        $res = $preparedStatement->execute(
            [
                ':heureDebutRencontre' => $entity->getHeureDebutRencontre()->format('Y-m-d H:i:s'),
                ':heureFinRencontre' => $entity->getHeureFinRencontre()->format('Y-m-d H:i:s'),
                ':idCompet' => $entity->getIdCompet()
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdCompet($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        echo $this->bdd->errorInfo()[2];

        return $resultSet;
    }

    /*Fonction de mise à jour d'un club
     * @param Club $entity
     * @return Club|null
     */
    public function update(Rencontre $entity): ?Rencontre {
        $resultSet = NULL;
        // On exécute l'update que si $entity a bien un numAct et que ce dernier existe en BDD
        if (is_null($entity->getIdRencontre()) || is_null($this->getById($entity->getIdRencontre()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE recontre"
                . " SET heureDebutRencontre=:heureDebut, "
                . " heureFinRencontre=:heureFin, "
                . " idCompet=:idCompet,"
                . " WHERE idRencontre = :id";

            // on prepare la requête
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Club');
            $res = $reqPrep->execute(
                [
                    ':heureDebut' => $entity->getHeureDebutRencontre(),
                    ':heureFin' => $entity->getHeureFinRencontre(),
                    ':idCompet' => $entity ->getIdCompet(),
                    ':id' => $entity->getIdRencontre()
                ]
            );

            if ($res !== FALSE) {
                // si tout c'est bien passé on met l'entité qui viens d'être mis à jour dans resultSet
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    /**
     * Fait un insert ou un update du site selon la clé du Club
     * @param rencontre $entity
     * @return array|null
     */
    function save(rencontre $entity): ?Rencontre {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getIdRencontre())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }

}