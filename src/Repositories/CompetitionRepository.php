<?php

namespace Repositories;

use Entities\Competition;

class CompetitionRepository {

    protected $bdd;

    public function __construct(\PDO $bdd) {
        if(!is_null($bdd)) $this->bdd = $bdd;
    }

    public function getAll() : ? array{
        $resultSet = NULL;
        $query = 'SELECT * FROM Competition;';

        $rqtResult = $this->bdd->query($query);

        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new Competition($row);
            }
        }

        return $resultSet;
    }

    public function getById( int $id): ?Competition {
        $resultSet = NULL;
        $query = 'SELECT * FROM Competition WHERE idCompet =:idCompet;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idCompet' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                $resultSet = new Competition($tab);
            }
        }
        return $resultSet;
    }

    
    public function insert(Competition $entity): ?Competition {
        $resultSet = NULL;

        $query = "INSERT INTO Competition(dateDebutCompet, dateFinCompet, idClubOrganisateur) VALUES (:dateDebutCompet, :dateFinCompet, :idClubOrganisateur)";
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':dateDebutCompet' => $entity->getDateDebutCompet()->format('Y-m-d H:i:s'),
                ':dateFinCompet' => $entity->getDateFinCompet()->format('Y-m-d H:i:s'),
                ':idClubOrganisateur' => $entity->getIdClubOrganisateur()
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdCompet($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        echo $reqPrep->errorInfo()[2];

        return $resultSet;
    }

    public function update(Competition $entity): ?Competition {
        $resultSet = NULL;
        if (is_null($entity->getIdCompet()) || is_null($this->getById($entity->getIdCompet()))) {
            $resultSet = NULL;
        } else {
            $query = "UPDATE competition SET dateDebutCompet=:dateDebutCompet, dateFinCompet=:dateFinCompet, idClubOrganisateur=:idClubOrganisateur WHERE idCompet = :idCompet";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Competition');
            $res = $reqPrep->execute(
                [
                    ':dateDebutCompet' => $entity->getDateDebutCompet()->format('Y-m-d H:i:s'),
                    ':dateFinCompet' => $entity->getDateFinCompet()->format('Y-m-d H:i:s'),
                    ':idClubOrganisateur' => $entity->getIdClubOrganisateur(),
                    ':idCompet' => $entity->getIdCompet()
                ]
            );

            dump_var($res, DUMP, '$res dans update Competition');
            echo $reqPrep->errorInfo()[2];

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    public function delete(Competition $entity): bool {
        if (is_null($entity->getIdCompet()) || is_null($this->getById($entity->getIdCompet()))) return false;

        else {
            $rencontreRepository = new RencontreRepository($this->bdd);
            (array) $rencontres = $rencontreRepository->getRencontreByCompetition($entity->getIdCompet());

            foreach ($rencontres as $rencontre) {
                $query = "DELETE FROM Participer WHERE idRencontre = :iRencontre";
                $preparedStatement = $this->bdd->prepare($query);
                $preparedStatement->execute([':iRencontre' => $rencontre->getIdRencontre()]);
            }

            $query = "DELETE FROM Rencontre WHERE idCompet = :idCompet";
            $preparedStatement = $this->bdd->prepare($query);
            $res = $preparedStatement->execute([':idCompet' => $entity->getIdCompet()]);

            $query = "DELETE FROM Competition WHERE idCompet = :idCompet";
            $preparedStatement = $this->bdd->prepare($query);
            $res = $preparedStatement->execute([':idCompet' => $entity->getIdCompet()]);

            return true;
        }
    }

    /**
     * Fait un insert ou un update du site selon la clé du Competition
     * @param Competition $entity
     * @return array|null
     */
    function save(Competition $entity): ?Competition {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getIdCompet())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }


}