<?php

namespace Repositories;

use PDO;

class UserRepository {

    private $bdd;

    /**
     * Function that send a request to the database in order to add a new user
     * @param \PDO $bdd : the database connection
     * @param array $tab : contains the user's informations
     * @return bool if the request is successful
     */
    function registerUser(array $tab):bool{
        $result = false;

        $query = "INSERT INTO user (username, password) VALUES (:username, :password);";
        $preparedStatement = $this->bdd->prepare($query);
        $res = $preparedStatement->execute(
            [':username' => $tab['username'],
                ':password' => $tab['password']
            ]
        );

        if ($res) $result = true;

        return $result;
    }

    /**
     * Function that check if the user is in the database
     * @param \PDO $bdd - array of the database info
     * @param array $tab - array of the user info (login and pass)
     * @return array - the user info if the user is in the database, null otherwise
     */
    function manageUser(array $tab):?array {
        $result = null;

        $query = 'SELECT * FROM user where username = :username;';
        $preparedStatement = $this->bdd->prepare($query);
        $res = $preparedStatement->execute(
            [':username' => $tab['username']]
        );

        if ($res) {
            $result =  ($tmp = $preparedStatement->fetch(PDO::FETCH_ASSOC)) ? $tmp : null;
            if(isset($result['password'])){
                if (password_verify($tab['password'], $result['password'])) return $result;
                else return null;
            }
        }

        return null;
    }

    public function __construct($bdd) {
        $this->bdd = $bdd;
    }

}