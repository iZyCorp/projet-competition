<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';


$rencontre = null;
$idRencontre=null;

$HeureDebutRencontre = NULL;
$HeureFinRencontre = NULL;
$idCompet= NULL;


//$idRencontre =$_GET ['$idRencontre'];//

use Repository\rencontreRepository;

if($idRencontre !== false && !is_null($idRencontre)) {
    //Connexion à la BDD
    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        $repo = new Repositories\rencontreRepository($bdd);

        $rencontre = $repo->getById(intval($idRencontre));

        if ($rencontre!== NULL) {
            $HeureDebutRencontre = $rencontre->getHeureDebutRencontre();
            $HeureFinRencontre = $rencontre->getHeureFinRencontre();
            $idCompet = $rencontre->getidCompet();

        }
    }
}
?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> Rencontre </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
?>

<section id="corps">
    <h1> Ajouter une rencontre </h1>
    </header>

    <form method="POST" action="../traits/traitRencontre.php">
        <input type="hidden" id="idRencontre" name="idRencontre" value="<?= $idRencontre ?>" /> <!--Permet de garder l'id quand on envoie les données du formulaire !-->
        <div>
            <label for="start">Date de la rencontre:</label><br>

            <input type="date" id="start" name="trip-start"
                   value="2022-12-22"
                   min="2022-01-01" max="2022-12-31">
        </div>
        </br>
        <div>
            <label for="HeureDebutRencontre">Heure de debut de la rencontre:</label><br/>

            <input type="time" id="HeureDebutRencontre" name="HeureDebutRencontre" value="<?= $HeureDebutRencontre ?>" size="40" min="09:00" max="18:00" required>

        </div>
        <div>
            <label for="HeureFinRencontre">Heure de debut de la rencontre:</label><br/>

            <input type="time" id="HeureFinRencontre" name="HeureFinRenconter" value="<?= $HeureFinRencontre ?>" size="40" min="09:00" max="18:00" required>
            <br/>
        </div>
        <div class="form-group">
            <button type="submit">Valider</button>
        </div>


    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>

