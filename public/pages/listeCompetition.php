<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if ($db) {
    $repo = new Repositories\CompetitionRepository($db);

    $lesCompete = $repo->getAll();
} else {
    $lesCompete = null;
}

?>

<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> Récupération des compétitions </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">

</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>
<section id="corps">

    <h1 class="embeded-title">Liste des Compétitions</h1>

    <?php if (!is_null($lesCompete)): ?>
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Date début</th><th>Date fin</th><th>id Club Organisateur</th><th>Rencontres</th><th>Ajouter Rencontre</th><th>Editer</th><th>Supprimer</th></tr>
            </thead>
            <tbody>
            <?php
            foreach ($lesCompete as $competition):
                ?>
                <tr>
                    <td> <?= $competition->getIdCompet(); ?> </td>
                    <td id="colonneLargeur2"><?= $competition->getDateDebutCompet()->format('Y-m-d h:m:s'); ?></td>
                    <td id="colonneLargeur2"><?= $competition->getDateFinCompet()->format('Y-m-d h:m:s') ?></td>
                    <td><?= $competition->getIdClubOrganisateur() ?></td>
                    <td><a href="listeRencontre.php?idCompet=<?= $competition->getIdCompet() ?>"> <img src="../img/listRencontre.png" alt="rencontre" class="little-img" width="55"/></td>
                    <td><a href="formInsertRencontre.php?idCompet=<?= $competition->getIdCompet() ?>"> <img src="../img/add.png" alt="rencontre" class="little-img" width="55"/></td>
                    <td><a href="formEditCompetition.php?idCompet=<?= $competition->getIdCompet() ?>"> <img src="../img/edit.png" alt="edit" class="little-img" width="35"/></a></td>
                    <td><a href="../traits/traitDeleteCompetition.php?idCompet=<?= $competition->getIdCompet() ?>"> <img src="../img/bin.png" alt="delete" class="little-img" width="35"/></a></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
        <p class="error-message">Il n'y a actuellement aucune compétition de planifié</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>