<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.

$club = null;
$idClub=null;

?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Partie 2 </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/formPageFooter.css">
</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>

<section id="corps">
    <h1> Ajouter un Club </h1> <!--A modifier pour la partie Optimisation !-->

    <form method="POST" action="../traits/traitInsertClub.php">
        <div>
            <label for="nomClub">Nom du club :</label><br/>
            <input type="text" id="nomClub" placeholder="nom du club" name="nomClub" size="40">
        </div>
        <div>
            <label for="adresseClub">Adresse du club :</label><br/>
            <input type="text"id="adresseClub" placeholder="Adresse du club" name="adresseClub" size="100" required="required">
        </div>
        <div>
            <label for="cpClub">Code postal du club :</label><br/>
            <input type="text"id="cpClub" placeholder="Code postal du club" name="cpClub" size="20" required="required">
        </div>
        <div>
            <label for="villeClub">Ville du club :</label><br/>
            <input type="text" id="villeClub" placeholder="Ville du club" name="villeClub" size="40" required="required">
        </div>
        <br/>
        <div class="form-group">
            <button type="submit"> Ajouter</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>
</body>
</html>

