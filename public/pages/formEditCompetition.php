<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

(int) $idCompet = intval($_GET['idCompet']);
$competition = null;
$dateDebutCompet = null;
$dateFinCompet = null;
$idClubOrganisateur = null;

    // Connecting to the database
    $bdd = connectBdd($infoBdd);
    // If bdd connection is ok
if ($bdd) {
    // Making the repository
    $repositoryCompet = new \Repositories\CompetitionRepository($bdd);
    // Recover the proper competition
    $competition = $repositoryCompet->getById($idCompet);
    // Defining variables from competition object
    $dateDebutCompet = $competition->getDateDebutCompet()->format('m-d-Y');
    $dateFinCompet = $competition->getDateFinCompet()->format('m-d-Y');
    $idClubOrganisateur = $competition->getIdClubOrganisateur();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/formPageFooter.css">
    <title>Title</title>
</head>
<?php include_once '../inc/header.php'; ?>
<body>

    <section id="corps">

        <h1> Edition de compétition </h1>

        <form method="POST" action="../traits/traitEditCompetition.php">
            <input type="hidden" name="idCompet" value="<?php echo $idCompet; ?>">

            <label for="dateDebutCompet">Date début:</label>
            <input type="date" name="dateDebutCompet" id="dateDebutCompet" value="<?php echo date('Y-m-d', strtotime($dateDebutCompet)); ?>">

            <label for="dateFinCompet">Date fin:</label>
            <input type="date" name="dateFinCompet" id="dateFinCompet" value="<?php echo date('Y-m-d', strtotime($dateFinCompet)); ?>">

            <label for="idClubOrganisateur">Club organisateur:</label>
            <input type="number" name="idClubOrganisateur" id="idClubOrganisateur" value="<?php echo $idClubOrganisateur; ?>" style="font-family: 'Roboto',serif">
            <input type="submit" value="Edit">
        </form>
    </section>

</body>
<?php include_once '../inc/footer.php'; ?>
</html>