<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if ($db) {
    $repositoryRencontre = new Repositories\RencontreRepository($db);

    $lesRencontres = $repositoryRencontre->getAll();
} else {
    $lesRencontres = null;
}

?>

<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> Récupération des compétitions </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">

</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>
<section id="corps">

    <h1 class="embeded-title">Liste des Rencontres</h1>

    <?php if (!is_null($lesRencontres)): ?>
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Date début</th><th>Date fin</th><th>id compétition</th><th>Tireur</th><th>Edit</th><th>Suprimmer</th></tr>
            </thead>
            <tbody>
            <?php
            foreach ($lesRencontres as $rencontre):
                ?>
                <tr>
                    <td> <?= $rencontre->getIdCompet(); ?> </td>
                    <td id="colonneLargeur2"><?= $rencontre->getHeureDebutRencontre()->format('Y-m-d H:i:s'); ?></td>
                    <td id="colonneLargeur2"><?= $rencontre->getHeureFinRencontre()->format('Y-m-d H:i:s') ?></td>
                    <td><?= $rencontre->getIdCompet() ?></td>
                    <td><a href="pageChoixTireur.php?idRencontre=<?= $rencontre->getIdRencontre() ?>"> <img src="../img/listRencontre.png" alt="rencontre" class="little-img" width="55"/></td>
                    <td><a href="formEditRencontre.php?idRencontre=<?= $rencontre->getIdRencontre() ?>"> <img src="../img/edit.png" alt="edit" class="little-img" width="35"/></a></td>
                    <td><a href="../traits/traitDeleteRencontre.php?idRencontre=<?= $rencontre->getIdRencontre() ?>"> <img src="../img/bin.png" alt="delete" class="little-img" width="35"/></a></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
        <p class="error-message">Il n'y a actuellement aucune rencontre planifié</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>