<?php require_once '../../config/appConfig.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>1SIO - TP PHP Partie 2</title>
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/index.css">
</head>
<?php include '../inc/header.php'; ?>
<body>
    <section id="title">
        <img src="../img/menu.png" alt="logo" id="logo" width="70" height="70">
        <h1>CompetitionManager</h1>
    </section>

    <section id="start">
        <p>Outil flexible, rapide et gratuit</p>
        <a href="formRegister.php">Démarrer</a>
    </section>

    <section id="body">
        <section class="floating-box" id="floating1">
            <h2>Clubs</h2>
            <p>Gérez vos clubs simplement grâce à notre outil performant</p>
            <p>Ajouter, supprimer ou éditez des clubs comme bon vous semble</p>
        </section>

        <section class="floating-box">
            <h2>Compétition</h2>
            <p>Plannifiez, supprimez ou éditer rapidement les compétitions de votre clubs</p>
            <p>Organisez et supervisez les rencontres des différents clubs</p>
        </section>
    </section>

</body>
<?php include_once '../inc/footer.php'; ?>
</html>


