<?php require_once '../../config/appConfig.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/connection.css">
</head>
<body>
    <section id="connection">

        <h1>Connexion</h1>

        <form action="../traits/traitConnection.php" method="post">
            <input type="text" name="username" placeholder="username" required>
            <input type="password" name="password" placeholder="password" required>
            <input type="submit" value="submit">
        </form>


        <a href="formRegister.php">Pas de compte?</a>
    </section>
</body>
</html>
