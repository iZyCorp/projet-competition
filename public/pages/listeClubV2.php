<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if ($db) {
    $lesClubs = getAllClubs($db);
} else {
    $lesClubs = null;
}

?>

<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Exercice 2 - Recupérer les clubs </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">

</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>
<section id="corps">

    <h1 class="embeded-title">Liste des clubs</h1>

    <?php if (!is_null($lesClubs)): ?>
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Nom du club</th><th>Adresse du club</th><th>Code postal</th><th>Ville</th><th>Editer</th></tr>
            </thead>
            <tbody>
            <?php
            foreach ($lesClubs as $value):
                ?>
                <tr>
                    <td> <?= $value['idClub']; ?> </td>
                    <td id="colonneLargeur2"><?= $value['nomClub']; ?></td>
                    <td id="colonneLargeur2"><?= $value['adresseClub']; ?></td>
                    <td><?= $value['cpClub']; ?></td>
                    <td id="colonneLargeur3"><?= $value['villeClub']; ?></td>
                    <td><a href="formEditClub.php?idClub=<?= $value['idClub'] ?>"> <img src="../img/edit.png" alt="Modifier" width="55"/> </a></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php include_once '../inc/footer.php'; ?>
</body>
</html>