<?php require_once '../../config/appConfig.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/formPageFooter.css">
    <title>Title</title>
</head>
<?php include_once '../inc/header.php'; ?>
<body>
    <section id="corps">
        <h1 class="embeded-title"> Ajouter une rencontre </h1>

        <form action="../traits/traitInsertRencontre.php" method="POST">
            <input type="hidden" id="idRencontre" name="idRencontre" /> <!--Permet de garder l'id quand on envoie les données du formulaire !-->

            <label for="heureDebutRencontre">Heure de debut de la rencontre:</label><br/>
            <input type="date" id="heureDebutRencontre" name="heureDebutRencontre" size="40" min="09:00" max="18:00" required>

            <label for="heureFinRencontre">Heure de Fin de la rencontre:</label><br/>
            <input type="date" id="heureFinRencontre" name="heureFinRencontre" size="40" min="09:00" max="18:00" required>

            <label for="idCompet">Numero de la Competition :</label><br/>
            <input type="number" name="idCompet" id="idCompet">

            <input type="submit" value="Ajouter">
        </form>
    </section>
</body>
<?php include_once '../inc/footer.php'; ?>
</html>

