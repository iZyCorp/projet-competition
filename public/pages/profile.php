<?php require_once '../../config/appConfig.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/profile.css">
</head>
<?php include_once '../inc/header.php'; ?>
<body>
    <section id="profile">

        <h1 class="embeded-title">Information Utilisateur</h1>

        <div class="container">
            <img src="../img/user.png" width="150" height="150" alt="user">

            <div id="user-info">
                <p>Nom: [Inconnu]</p>
                <p>Prénom: [Inconnu]</p>
                <p>Pseudonyme: [Inconnu]</p>
            </div>
        </div>


    </section>
</body>
<?php include_once '../inc/footer.php'; ?>
</html>
