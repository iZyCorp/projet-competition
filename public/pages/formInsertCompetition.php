<?php use Repositories\ClubRepository;

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$bdd = connectBdd($infoBdd);

$clubRepository = new ClubRepository($bdd);
$clubs = $clubRepository->getAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/formPageFooter.css">
    <title>Title</title>
</head>
<?php include_once '../inc/header.php'; ?>
<body>
    <section id="corps">
        <h1> Ajouter une compétition </h1>

        <form action="../traits/traitAjoutCompetition.php" method="post">
            <input type="date" name="dateDebutCompet" placeholder="Date début" value="" required>
            <input type="date" name="dateFinCompet" placeholder="Date fin" required>
            <select name="idClubOrganisateur" id="idClubOrganisateur" required>
                <option value="">Choisissez le club organisateur</option>
                <?php foreach ($clubs as $club) {?>
                 <option value="<?php echo $club->getIdClub() ?>"> <?php echo $club->getNomClub() ?></option>
                 <?php } ?>
            </select>
            <input type="submit" value="Ajouter">
        </form>
    </section>
</body>
<?php include_once '../inc/footer.php'; ?>
</html>