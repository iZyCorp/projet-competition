<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

use Repositories\TireurRepository;
use Repositories\ClubRepository;
use Repositories\NiveauTireurRepository;

use Entities\NiveauTireur;
use Entities\Club;

if ($db) {
    $repoTireur = new Repositories\TireurRepository($db);
    $repoClub = new Repositories\ClubRepository($db);
    $repoNivTireur = new Repositories\NiveauTireurRepository($db);

    $listeTireurs = $repoTireur->getAll();

} else {
    $listeTireurs = null;
}

?>

<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Exercice 7 - Recupérer les tireurs</TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>
<section id="corps">

    <h1 class="embeded-title">Liste des tireurs</h1>

    <?php if (!is_null($listeTireurs)): ?>
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Nom du tireur</th><th>Prenom du tireur</th><th>Date Naissance du tireur</th><th>Numéro licence tireur</th><th>Sexe du tireur</th><th>Poids du tireur</th><th>Club du tireur</th><th>Niveau du tireur</th><th>Editer</th></tr>
            </thead>
            <tbody>
            <?php
            foreach ($listeTireurs as $value):

                $clubTireur = $repoClub->getById($value->getIdClub());
                $niveauTireur = $repoNivTireur->getById($value->getIdNivTireur());

                ?>
                <tr>
                    <td> <?= $value->getIdTireur(); ?> </td>
                    <td id="colonneLargeur2"><?= $value->getNomTireur(); ?></td>
                    <td id="colonneLargeur2"><?= $value->getPrenomTireur(); ?></td>
                    <td><?= $value->getDateNaissTireur(); ?></td>
                    <td><?= $value->getNumLicenceTireur(); ?></td>
                    <td><?= $value->getSexeTireur(); ?></td>
                    <td><?= $value->getPoidsTireur(); ?></td>
                    <td><?= $clubTireur->getNomClub(); ?></td>
                    <td><?= $niveauTireur->getLibNivTireur(); ?></td>
                    <td><a href="../pages/formEditTireurPOO.php?idTireur=<?= $value->getIdTireur(); ?>"> <img src="../img/edit.png" alt="Modifier" width="55"/> </a></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>