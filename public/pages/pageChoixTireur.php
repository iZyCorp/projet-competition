 <?php
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';
$bdd = connectBdd($infoBdd);
if (!is_null($bdd)) {
    $repoTireur = new Repositories\TireurRepository($bdd);
    $resTireur = $repoTireur->getAll();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title> TP PHP Partie 2 </title>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
</head>
<BODY>
<?php
include_once '../inc/header.php';
?>
<section id="corps">

    <h1 class="embeded-title">Choix Tireur</h1><br>

    <form method="POST" action="../traits/traitUpdateRencontre.php">

        <label for="tireur" style="color: #000059">Bleu :</label>
        <select name="tireur" id="tireur">
            <?php foreach ($resTireur as $tireur) { ?>
            <option value='<?= $tireur->getIdTireur();?>'> <?=$tireur->getNomTireur(); ?></option>
            <?php } ?>
        </select>

        <label for="tireur" style="color: #830000"> Rouge : </label>
        <select name="tireur" id="tireur">
            <?php foreach ($resTireur as $tireur) {?>
                <option value='<?= $tireur->getIdTireur();?>'> <?=$tireur->getNomTireur(); ?></option>
            <?php } ?>
        </select>

        <select name="gagnant" id="gagnant">
            <option value="bleu">Bleu</option>
            <option value="rouge">Rouge</option>
        </select>

        <input type="submit" value="Enregister">
    </form>
</section>
</BODY>
</html>