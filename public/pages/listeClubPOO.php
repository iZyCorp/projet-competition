<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

use Entities\Club;

if ($db) {
    $repo = new Repositories\ClubRepository($db);

    $lesClubs = $repo->getAll();
} else {
    $lesClubs = null;
}

?>

<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Exercice 2 - Recupérer les clubs POO</TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">

</HEAD>
<BODY>
<?php include_once '../inc/header.php'; ?>
<section id="corps">
    <h1>LISTES DES CLUBS</h1>
    <p>Un petit extrait de notre base de données </p>
    <?php if (!is_null($lesClubs)): ?>
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Nom du club</th><th>Adresse du club</th><th>Code postal</th><th>Ville</th><th>Editer</th></tr>
            </thead>
            <tbody>
            <?php
            foreach ($lesClubs as $value):
                ?>
                <tr>
                    <td> <?= $value->getIdClub(); ?> </td>
                    <td id="colonneLargeur2"><?= $value->getNomClub(); ?></td>
                    <td id="colonneLargeur2"><?= $value->getAdresseClub(); ?></td>
                    <td><?= $value->getCpClub(); ?></td>
                    <td id="colonneLargeur3"><?= $value->getVilleClub(); ?></td>
                    <td><a href="formEditClubPOO.php?idClub=<?= $value->getIdCLub(); ?>"> <img src="../img/edit.png" alt="Modifier"/> </a></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>