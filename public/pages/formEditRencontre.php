<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.
(int) $idRencontre = intval($_GET['idRencontre']);
$rencontre  = null;
$heureDebutRencontre=null;
$heureFinRencontre=null;
$idCompet=null;

//Connexion à la BDD
    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        $rencontreRepository= new \Repositories\rencontreRepository($bdd);
// Recover the proper competition
        $rencontre = $rencontreRepository->getById($idRencontre);
// Defining variables from rencontre object
        $heureDebutRencontre = $rencontre->getHeureDebutRencontre()->format('m-d-Y');
        $heureFinRencontre = $rencontre->getHeureFinRencontre()->format('m-d-Y');
        $idCompet =$rencontre->getIdCompet();


}
var_dump($heureDebutRencontre,);
var_dump($heureFinRencontre);
?>
<!DOCTYPE html>
<html>
<head>
    <title> InsertRencontre </title>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/common.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/form.css">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/formPageFooter.css">
</head>
<?php include_once '../inc/header.php'; ?>
<body>
<section id="corps">

    <h1 class="embeded-title"> Edit une rencontre </h1>

    <form action="../traits/traitInsertRencontre.php" method="POST">

        <input type="hidden" id="idRencontre" name="idRencontre" value="<?php echo $idRencontre ?>" /> <!--Permet de garder l'id quand on envoie les données du formulaire !-->

        <label for="heureDebutRencontre">Heure de debut de la rencontre:</label><br/>
        <input type="date" id="heureDebutRencontre" name="heureDebutRencontre" value="<?php echo date('Y-m-d', strtotime($heureDebutRencontre)); ?>" size="40" min="09:00" max="18:00" required>

        <label for="heureFinRencontre">Heure de Fin de la rencontre:</label><br/>
        <input type="date" id="heureFinRencontre" name="heureFinRencontre" value="<?php echo date('Y-m-d', strtotime($heureFinRencontre)); ?>" size="40" min="09:00" max="18:00" required>

        <label for="idCompet">Numero de la Competition :</label><br/>
        <input type="number" name="idCompet" id="idCompet" value="<?php echo $idCompet; ?>">

        <input type="submit" value="Ajouter">
    </form>
</section>
</body>
<?php include_once '../inc/footer.php'; ?>
</html>

