<link rel="stylesheet" href="../css/header.css">
<header>
    <ul>
        <li id="menu-logo"><img src="../img/menu.png" width="40" alt="logo"></li>
        <li class="dropdown"><a href="index.php">Accueil</a></li>
        <?php
        if (isset($_SESSION['account'])) {
            echo '<li class="dropdown"><a href="profile.php">Profil</a>';
            echo '<div class="dropdown-content">';
            echo '<a href="../traits/traitLogout.php">Déconnexion</a></div>';
        } else echo '<li class="dropdown"><a href="formConnection.php">Connexion</a></li>';
        ?>
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropbtn">Exercices</a>
            <div class="dropdown-content">
                <a href="listeClubs.php">Les clubs (Ex 2)</a>
                <a href="listeClubV2.php">Les Clubs (Ex 4)</a>
                <a href="listeTireursPOO.php">Les tireurs</a>
            </div>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropbtn">Clubs</a>
            <div class="dropdown-content">
                <?php if (isset($_SESSION['account']))
                 echo '<a href="../../tests/insertClub.php">Ajouter un club</a>';
                ?>
            </div>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropbtn">Compétitions</a>
            <div class="dropdown-content">
                <a href="listeCompetition.php">Afficher les compétitions</a>
               <?php if (isset($_SESSION['account']))
                    echo '<a href="../pages/formInsertCompetition.php">Ajouter une compétition</a>';
               ?>
            </div>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropbtn">Rencontre</a>
            <div class="dropdown-content">
                <a href="listeRencontre.php">Liste Des Rencontres</a>
                <?php if (isset($_SESSION['account']))
                    echo '<a href="formInsertRencontre.php">Ajouter une Rencontre</a>'; ?>
            </div>
        </li>

        <li class="dropdown">
            <a href="javascript:void(0)" class="dropbtn">Tireurs</a>
            <div class="dropdown-content">
                <?php if (isset($_SESSION['account']))
                    echo '<a href="../pages/pageChoixTireur.php">Choix tireurs</a>'; ?>
            </div>
        </li>
    </ul>
</header>