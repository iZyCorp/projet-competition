<?php
declare(strict_types=1);

use Repositories\CompetitionRepository;

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    dump_var($_POST, DUMP, '$_POST');

    $filters = array(
        'idCompet' => FILTER_VALIDATE_INT,
        'dateDebutCompet' => FILTER_SANITIZE_STRING,
        'dateFinCompet' => FILTER_SANITIZE_STRING,
        'idClubOrganisateur' => FILTER_SANITIZE_NUMBER_INT
    );

    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);

    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        $competitionRepository = new CompetitionRepository($bdd);
        $competitionRepository->update(new \Entities\Competition($postFiltre));
    }
}

header("location: ../pages/listeCompetition.php");