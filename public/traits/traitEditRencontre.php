<?php
declare(strict_types=1);

use Repositories\recontreRepository;

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    dump_var($_POST, DUMP, '$_POST');

    $filters = array(
        'idRencontre' => FILTER_VALIDATE_INT,
        'heureDebutRencontre' => FILTER_SANITIZE_STRING,
        'heureFinRencontre ' => FILTER_SANITIZE_STRING,
        'idCompet' => FILTER_SANITIZE_NUMBER_INT
    );

    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);

    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        $recontreRepository = new recontreRepository($bdd);
        $recontreRepository->update(new \Entities\rencontre($postFiltre));
    }
}

header("location: ../pages/listeRencontre   .php");