<?php
declare(strict_types=1);

use Repositories\CompetitionRepository;

define('DUMP', true);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$dateDebutCompet = filter_input(INPUT_POST,'dateDebutCompet',FILTER_SANITIZE_STRING);
$dateFinCompet = filter_input(INPUT_POST,'dateFinCompet',FILTER_SANITIZE_STRING);
$idClubOrganisateur = filter_input(INPUT_POST,'idClubOrganisateur',FILTER_SANITIZE_STRING);

dump_var($dateDebutCompet);
dump_var($dateFinCompet);
//Connexion à la BDD
$bdd = connectBdd($infoBdd);
if (!is_null($bdd)) {
    $competitionRepository = new CompetitionRepository($bdd);
    $competArray = array(
        'dateDebutCompet' => $dateDebutCompet,
        'dateFinCompet' => $dateFinCompet,
        'idClubOrganisateur' => intval($idClubOrganisateur));

    dump_var($competArray);

    $competition = $competitionRepository->insert(new \Entities\Competition($competArray));
    header("location: ../pages/listeCompetition.php");
} else {
    header("location: ../pages/formInsertCompetition.php");
}
