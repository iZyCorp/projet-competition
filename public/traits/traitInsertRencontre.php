<?php
declare(strict_types=1);

use Repositories\RencontreRepository;

define('DUMP', true);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$heureDebutRencontre = filter_input(INPUT_POST,'heureDebutRencontre',FILTER_SANITIZE_STRING);
$heureFinRencontre = filter_input(INPUT_POST,'heureFinRencontre',FILTER_SANITIZE_STRING);
$idCompet = filter_input(INPUT_POST,'idCompet',FILTER_SANITIZE_NUMBER_INT);

dump_var($heureDebutRencontre);
dump_var($heureFinRencontre);
//Connexion à la BDD
$bdd = connectBdd($infoBdd);
if (!is_null($bdd)) {
    $recontreRepository = new rencontreRepository($bdd);
    $rencontreArray = array(
        'heureDebutRencontre' => $heureDebutRencontre,
        'heureFinRencontre' => $heureFinRencontre,
        'idCompet' => $idCompet);

    dump_var($rencontreArray);

    $rencontre = $recontreRepository->insert(new \Entities\rencontre($rencontreArray));
    //header("location: ../pages/listeRencontre.php");
} else {
    //header("location: ../pages/formInsertRencontre.php");
}
