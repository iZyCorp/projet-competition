<?php

use Repositories\CompetitionRepository;

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$idCompetToDelete = $_GET['idCompet'];

$bdd = connectBdd($infoBdd);
$competitionRepository = new CompetitionRepository($bdd);
$competToDelete = $competitionRepository->getById($idCompetToDelete);
$competitionRepository->delete($competToDelete);

header("location: ../pages/listeCompetition.php");