<?php
declare(strict_types=1);

define('DUMP', true);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$nom = filter_input(INPUT_POST,'nomClub',FILTER_SANITIZE_STRING);
$adresse = filter_input(INPUT_POST,'adresseClub',FILTER_SANITIZE_STRING);
$cp = filter_input(INPUT_POST,'cpClub',FILTER_SANITIZE_STRING);
$ville = filter_input(INPUT_POST,'villeClub',FILTER_SANITIZE_STRING);

//Connexion à la BDD
$bdd = connectBdd($infoBdd);
if (!is_null($bdd)) {
    $clubCreate = array('nomClub' => $nom, 'adresseClub' => $adresse, 'cpClub' => $cp, 'villeClub' => $ville);
    $club = insertClub($bdd, $clubCreate);
    header("location: ../pages/listeClubs.php");
} else {
    header("location: ../pages/formInsertClub.php");
}
