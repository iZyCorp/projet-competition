<?php

use Repositories\UserRepository;

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

if ('POST' === $_SERVER['REQUEST_METHOD']) {

    dump_var($_POST, DUMP, '$_POST');

    $filtres = array (
        'username'=> FILTER_SANITIZE_STRING,
        'password'=> FILTER_SANITIZE_STRING,
    );

    $postFiltre = filter_input_array(INPUT_POST, $filtres, false);

    $bdd = connectBdd($infoBdd);
    if ($bdd) {

        $userRepository = new UserRepository($bdd);
        $result = $userRepository->manageUser($postFiltre);

        if ($result) {

            $_SESSION['account']=$result;
            header("location:../pages/index.php");
        }
    }
}
