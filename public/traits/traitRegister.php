<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

use Repositories\UserRepository;

define('DUMP', true);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';


if ('POST' === $_SERVER['REQUEST_METHOD']) {

    $filtres =array (
        'username'=> FILTER_SANITIZE_STRING,
        'password'=> FILTER_SANITIZE_STRING,
    );

    $postFiltre = filter_input_array(INPUT_POST, $filtres, TRUE);

    $motDePasse = password_hash($postFiltre['password'], PASSWORD_DEFAULT);

    $postFiltre['password']=$motDePasse;

    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        $userRepository = new UserRepository($bdd);
        $res = $userRepository->registerUser($postFiltre);
        header("location: ../pages/index.php");
    }
}